import React, { Component } from 'react';
import FileSaver from 'file-saver';
//import axios from 'axios';

class MainPage extends Component {

    constructor(props) {
        super(props);
        this.state = {value: ''};
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        }

        handleChange(event) {
        this.setState({value: event.target.value});
        }

        handleSubmit(event) {

        console.log('handleSubmit value:', this.state.value) ;   
        this.getLyric(this.state.value*1);
       // alert('A name was submitted: ' + this.state.value);
        event.preventDefault();
    }

    // CORS 정책에 의해 차단됨 : 요청 된 리소스에 'Access-Control-Allow-Origin'헤더가 없습니다
    //package에 proxy:...추가하였음
     getLyric = (song_id)=>{
        console.log('%c----------getLyric-------------','color:red');
        var queryString=`/readxtf/read_xtf.jsp?songNo=${song_id}`;
        fetch( queryString, { 
            method: 'GET', 
            mode: 'cors', // no-cors, cors, *same-origin
            headers: {
                'Content-Type': 'text/html; charset=UTF-8',
            },
            credentials: 'omit',  
        })
       .then(response => response.text())
    //    .then(response => {
    //       console.log("-----------------------response:",response);
    //    })

      .then(function(response) {
        const blob = new Blob([response], {type: "text/plain"});
        return blob;})
      .then(function(response) {
        FileSaver.saveAs(response, 'nameFile.txt');
      })


      };
    render() {
        return (
        <form onSubmit={this.handleSubmit}>
            <label>
            Song ID:
            <input type="text" value={this.state.value} onChange={this.handleChange} />
            </label>
            <input type="submit" value="RUN" />
        </form>
        );
    }
}


export default MainPage;